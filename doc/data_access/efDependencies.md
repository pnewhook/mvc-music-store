## Installing Entity Framework Dependencies

Entity Framework is not included by default in the ASP.NET MVC Empty Project template so we will have to add it ourselves using NuGet.

Ordinarily, we would open the NuGet package manager by right clicking on the MvcMusicStore project and selecting Manage NuGet Packages... 

![](manageNugetPackages.jpg)

However, at the time of authoring this tutorial, a new version of Entity Framework has been released (Entity Framework 6), and the GUI package manager only allows us to download the most recent version of package. We want to use EF5 because it better supports ASP.NET MVC 4. So open the Package Manager Console by clicking on Tools > NuGet Package Manager > Package Manager Console

![](openPackageManagerConsole.jpg)

Then issue the command:

```shell
Install-Package EntityFramework -Version 5.0.0
```

And Entity Framework will be downloaded and added to your project.

![](packageManagerConsole.jpg)