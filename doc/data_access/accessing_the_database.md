## Database access with Entity Framework Code-First

Entity Framework (EF) is the preferred technology to query and update the database. EF is a flexible object relational mapping (ORM) data API that enables developers to query and update data stored in a database in an object-oriented way.

Beginning in version 4, Entity Framework has supported a development paradigm called code-first. Code-first allows you to create model object by writing simple classes (also known as POCO from "plain-old" CLR objects), and can even create the database on the fly from your classes.

At the time of the ASP.NET MVC 4 release Entity Framework at version 4. However, at the time of authoring this tutorial, Entity Framework version 6 was available, so we're going to use that version instead.

### Changes to our Model Classes

We will be leveraging the database creation feature in Entity Framework in this tutorial. Before we do that, though, let’s make a few minor changes to our model classes to add in some things we’ll be using later on.

#### Adding the Artist Model Classes

Our Albums will be associated with Artists, so we’ll add a simple model class to describe an Artist. Add a new class to the Models folder named Artist.cs using the code shown below.

```cs
namespace MvcMusicStore.Models
{
    public class Artist
    {
        public int ArtistId { get; set; }
        public string Name { get; set; }
    }
}
```

#### Updating our Model Classes

Update the Album class as shown below.

```cs
namespace MvcMusicStore.Models
{
    public class Album
    {
        public int      AlbumId     { get; set; }
        public int      GenreId     { get; set; }
        public int      ArtistId    { get; set; }
        public string   Title       { get; set; }
        public decimal  Price       { get; set; }
        public string   AlbumArtUrl { get; set; }
        public Genre    Genre       { get; set; }
        public Artist   Artist      { get; set; }
    }
}
```

Next, make the following updates to the Genre class.

```cs
using System.Collections.Generic;
 
namespace MvcMusicStore.Models
{
    public partial class Genre
    {
        public int      GenreId     { get; set; }
        public string   Name        { get; set; }
        public string   Description { get; set; }
        public List<Album> Albums   { get; set; }
    }
}
```

### Creating a Connection String in the web.config file

We will add a few lines to the website’s configuration file so that Entity Framework knows how to connect to our database. Double-click on the Web.config file located in the root of the project.

![](webconfig.jpg)

Add a the following `<connectionStrings>` section directly after the configSections element.

```xml
  <connectionStrings>
    <add name="musicstoreconnection"
         providerName="System.Data.SqlClient"
         connectionString="Data Source=.\SQLEXPRESS;Initial Catalog=MvcMusicStore;Integrated Security=True;MultipleActiveResultSets=True"/>
  </connectionStrings>
```

### Adding a Context Class

Right-click the Models folder and add a new class named MusicStoreContext.cs.

![](solutionExplorerContext.jpg)

This class will represent the Entity Framework database context, and will handle our create, read, update, and delete operations for us. The code for this class is shown below.

```cs
using System.Data.Entity;

namespace MvcMusicStore.Models
{
    public class MusicStoreContext : DbContext
    {
        public MusicStoreContext()
            : base("musicstoreconnection")
        {
        }

        public DbSet<Album> Albums { get; set; }
        public DbSet<Genre> Genres { get; set; }
    }
}
```

That’s it - there’s no other configuration, special interfaces, etc. By extending the DbContext base class, our MusicStoreEntities class is able to handle our database operations for us. Now that we’ve got that hooked up, let’s add a few more properties to our model classes to take advantage of some of the additional information in our database.

### Adding our store catalog data

We will take advantage of a feature in Entity Framework which adds “seed” data to a newly created database. This will pre-populate our store catalog with a list of Genres, Artists, and Albums. The MvcMusicStore-Assets.zip download - which included our site design files used earlier in this tutorial - has a class file with this seed data, located in a folder named Code.

Within the Code / Models folder, locate the SeedingInitializer.cs file and drop it into the Models folder in our project, as shown below.

![](addSeedingInitializer.jpg)

Now we need to add one line of code to tell Entity Framework about that SeedingInitializer class. In the MusicStoreContext constructor, add the line below to tell Entity Framework to use your class as a database initializer.

```cs
public MusicStoreContext()
    : base("musicstoreconnection")
{
    Database.SetInitializer<MusicStoreContext>(new SeedingInitializer());
}
```

At this point, we’ve completed the work necessary to configure Entity Framework for our project.