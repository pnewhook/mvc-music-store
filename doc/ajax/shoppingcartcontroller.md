# The Shopping Cart Controller

The Shopping Cart controller has three main purposes: adding items to a cart, removing items from the cart, and viewing items in the cart. It will make use of the three classes we just created: ShoppingCartViewModel, ShoppingCartRemoveViewModel, and ShoppingCart. As in the StoreController and StoreManagerController, we’ll add a field to hold an instance of MusicStoreEntities.

Add a new Shopping Cart controller to the project using the Empty API controller template.

![](shoppingCartControllerDialog.jpg)

Here’s the complete ShoppingCart Controller. The Index and Add Controller actions should look very familiar. The Remove and CartSummary controller actions handle two special cases, which we’ll discuss in the following section.

