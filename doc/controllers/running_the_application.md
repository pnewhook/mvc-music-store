## Running the Application

Now let's run the site. We can start our web-server and try out the site using any of the following::

- Choose the Debug -> Start Debugging menu item
- Click the Green arrow button in the toolbar![](debug.jpg)
- Use the keyboard shortcut, F5.

Using any of the above steps will compile our project, and then cause IIS Express to start. To check what port the applicaiton is running under, you can right click on the IIS Express icon and expand the details for your site.

![](iisExpress.jpg)

Visual Studio will then automatically open a browser window whose URL points to our web-server. This will allow us to quickly try out our web application:

![](stringReturn.jpg)

Okay, that was pretty quick - we created a new website, added a three line function, and we've got text in a browser. Not rocket science, but it's a start.

> Note: Visual Studio includes IIS Express, which will run your website on a random free "port" number. In the screenshot above, the site is running at http://localhost:5166/, so it's using port 5166. Your port number will be different. When we talk about URL's like /Store/Browse in this tutorial, that will go after the port number. Assuming a port number of 5166, browsing to /Store/Browse will mean browsing to http://localhost:5166/Store/Browse.
