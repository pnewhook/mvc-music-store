## Adding a Store Controller

We added a simple HomeController that implements the Home Page of our site. Let's now add another controller that we'll use to implement the browsing functionality of our music store. Our store controller will support three scenarios:

- A listing page of the music genres in our music store
- A browse page that lists all of the music albums in a particular genre
- A details page that shows information about a specific music album

We'll start by adding a new StoreController class. If you haven't already, stop running the application either selecting the Debug -> Stop Debugging menu item or pressing Shift+F5.

Now add a new StoreController. Just like we did with HomeController, we'll do this by right-clicking on the "Controllers" folder within the Solution Explorer and choosing the Add->Controller menu item

![](addStoreController.jpg)



Our new StoreController already has an "Index" method. We'll use this "Index" method to implement our listing page that lists all genres in our music store. We'll also add two additional methods to implement the two other scenarios we want our StoreController to handle: Browse and Details.

These methods (Index, Browse and Details) within our Controller are called "Controller Actions", and as you've already seen with the HomeController.Index()action method, their job is to respond to URL requests and (generally speaking) determine what content should be sent back to the browser or user that invoked the URL.

We'll start our StoreController implementation by changing theIndex() method to return the string "Hello from Store.Index()" and we'll add similar methods for Browse() and Details():
```cs
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcMusicStore.Controllers
{
    public class StoreController : Controller
    {
        //
        // GET: /Store/
        public string Index()
        {
            return "Hello from Store.Index()";
        }
        //
        // GET: /Store/Browse
        public string Browse()
        {
            return "Hello from Store.Browse()";
        }
        //
        // GET: /Store/Details
        public string Details()
        {
            return "Hello from Store.Details()";
        }
    }
}
```

Run the project again and browse the following URLs:

- /Store
- /Store/Browse
- /Store/Details

Accessing these URLs will invoke the action methods within our Controller and return string responses:

![](storeDetails.jpg)

That's great, but these are just constant strings. Let's make them dynamic, so they take information from the URL and display it in the page output.

First we'll change the Browse action method to retrieve a querystring value from the URL. We can do this by adding a "genre" parameter to our action method. When we do this ASP.NET MVC will automatically pass any querystring or form post parameters named "genre" to our action method when it is invoked.

```cs
//
// GET: /Store/Browse?genre=Disco
public string Browse(string genre)
 {
    string message = HttpUtility.HtmlEncode("Store.Browse, Genre = "
+ genre);
 
    return message;
 }
```
> Note: We’re using the HttpUtility.HtmlEncode utility method to sanitize the user input. This prevents users from injecting Javascript into our View with a link like /Store/Browse?Genre=<script>window.location=’http://hackersite.com’</script>.

Now let’s browse to /Store/Browse?Genre=Disco:

![](browseQuerystring.jpg)

Let’s next change the Details action to read and display an input parameter named ID. Unlike our previous method, we won’t be embedding the ID value as a querystring parameter. Instead we’ll embed it directly within the URL itself. For example: /Store/Details/5.

ASP.NET MVC lets us easily do this without having to configure anything. ASP.NET MVC’s default routing convention is to treat the segment of a URL after the action method name as a parameter named “ID”. If your action method has a parameter named ID then ASP.NET MVC will automatically pass the URL segment to you as a parameter.

```cs
//
// GET: /Store/Details/5
public string Details(int id)
 {
    string message = "Store.Details, ID = " + id;
 
    return message;
 }
```

Run the application and browse to /Store/Details/5:

![](detailsId.jpg)

Let’s recap what we’ve done so far:
- We’ve created a new ASP.NET MVC project in Visual Web Developer
- We’ve discussed the basic folder structure of an ASP.NET MVC application
- We’ve learned how to run our website using the ASP.NET Development Server
- We’ve created two Controller classes: a HomeController and a StoreController
- We’ve added Action Methods to our controllers which respond to URL requests and return text to the browser