## Adding a Home Controller

Adding a HomeController

We’ll begin our MVC Music Store application by adding a Controller class that will handle URLs to the Home page of our site.
We’ll follow the default naming conventions of ASP.NET MVC and call it HomeController.

Right-click the “Controllers” folder within the Solution Explorer and select “Add”, and then the “Controller…” command:

![](addController.jpg)

This will bring up the “Add Controller” dialog. Name the controller “HomeController” and press the Add button.

![](addHomeController.jpg)


This will create a new file, HomeController.cs, with the following code:

```cs
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcMusicStore.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }

    }
}
```

To start as simply as possible, let’s replace the Index method with a simple method that just returns a string. We’ll make two changes:

- Change the method to return a string instead of an ActionResult
- Change the return statement to return “Hello from Home”

The method should now look like this:

```cs
public string Index()
{
    return "Hello from Home";
}

```
