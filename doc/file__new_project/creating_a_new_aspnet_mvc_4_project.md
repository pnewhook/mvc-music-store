## Creating a new ASP.NET MVC 4 project
We’ll start by selecting “New Project” from the File menu in Visual Web Developer. This brings up the New Project dialog.

![](fileNew.jpg)

We’ll select the Visual C# -> Web Templates group on the left, then choose the “ASP.NET MVC 4 Web Application” template in the center column. Name your project MvcMusicStore and press the OK button.

![](newProjectDialog.jpg)

This will display a secondary dialog which allows us to make some MVC specific settings for our project. Select the following:

- Project Template - Empty
- View Engine - Razor
- Create a unit test project - true
- Test project name - MvcMusicStore.Tests

![](projectTemplate.jpg)

This will create our project. Let’s take a look at the folders that have been added to our application in the Solution Explorer on the right side.

![](newProjectFileStructure.jpg)

| Folder | Purpose |
| -- | -- |
| /Controllers | Controllers respond to input from the browser, decide what to do with it, and return response to the user. |
| /Models | Models hold and manipulate data |
| /Views | Views hold our UI templates |
| /App_Start | Contains code that will be executed at the initialization of our website |
| /App_Data | A folder to store databases during development |

These folders are included even in an Empty ASP.NET MVC application because the ASP.NET MVC framework by default uses a “convention over configuration” approach and makes some default assumptions based on folder naming conventions. For instance, controllers look for views in the Views folder by default without you having to explicitly specify this in your code. Sticking with the default conventions reduces the amount of code you need to write, and can also make it easier for other developers to understand your project. We’ll explain these conventions more as we build our application.
