## Installing the Software
This tutorial will begin by creating a new ASP.NET MVC 3 project using the free Visual Web Developer 2010 Express (which is free), and then we’ll incrementally add features to create a complete functioning application. Along the way, we’ll cover database access, form posting scenarios, data validation, using master pages for consistent page layout, using AJAX for page updates and validation, user login, and more.

You can follow along step by step, or you can download the completed application from [https://bitbucket.org/pnewhook/mvc-music-store](https://bitbucket.org/pnewhook/mvc-music-store).

You can use either Visual Studio 2010 SP1 or Visual Web Developer 2010 Express SP1 (a free version of Visual Studio 2010) to build the application. We’ll be using the SQL Server Compact (also free) to host the database. Before you start, make sure you've installed the prerequisites listed below. You can install all of them using the following Web Platform Installer link: http://www.microsoft.com/web/handlers/webpi.ashx?command=getinstallerredirect&appid=VWD2010SP1Pack
