## Creating the StoreManagerController

In the past chapter, we were loading data from our database and displaying it. In this chapter, we’ll also enable editing the data.

We’ll begin by creating a new controller called StoreManagerController. For this controller, we will be taking advantage of the Scaffolding features of ASP.NET MVC. Set the options for the Add Controller dialog as shown below.

![](storemanagercontrollerDialog.jpg)

When you click the Add button, you’ll see that the ASP.NET MVC scaffolding mechanism does a good amount of work for you:

- It creates the new StoreManagerController with a local Entity Framework variable
- It adds a StoreManager folder to the project’s Views folder
- It adds Create.cshtml, Delete.cshtml, Details.cshtml, Edit.cshtml, and Index.cshtml view, strongly typed to the Album class

![](storeManagerScaffolding.jpg)

The new StoreManager controller class includes CRUD (create, read, update, delete) controller actions which know how to work with the Album model class and use our Entity Framework context for database access.