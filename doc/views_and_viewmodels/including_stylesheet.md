## Including StyleSheets and JavaScript Using Bundles

### Creating Style Bundles
Styling for the MVC Music Store has already been provided for you in source code from https://bitbucket.org/pnewhook/mvc-music-store. Create a Content folder at the root of the root of the web site and copy in Site.css and the img folder.

![](copyContent.jpg)

The Content folder of your application will now appear as follows:

![](solutionExplorerContent.jpg)

We can now add the CSS file to the common layout. While it's certainly possible to add a stylesheet to our page directly using the `<link>` tag, ASP.NET MVC 4 introduces a new feature called bundling and minification that makes web performance best practices easy.

Your page may include multiple CSS files to style your website, each requiring an HTTP request to the server. Additionally CSS files include a lot of additional content the browser doesn't need (i.e. comments and white-space). This extra overhead is a drag on performance and can slow your site down. The bundling and minificaiton features of ASP.NET MVC 4 take care of combining many CSS (or JavaScript) files into a single file (bundling) and remove unnecessary content (minification). A more in-depth discussion of the benefits of bundling and minification can be found at http://www.asp.net/mvc/overview/performance/bundling-and-minification.

Before we continue, you'll need to include the assemblies for bundling and minification. We will be using NuGet, the .Net package manager, to add the required dependencies. Right click on References under the MvcMusicStore project and select Manage NuGet Packages...

![](openNuget.jpg)

Search for "optimization" and install the Microsoft ASP.NET Web Optimization Framework.

![](optimizationInstall.jpg)

We need a class where we can configure the bundling and minification, so create a new class called BundleConfig inside the App_Start folder.

![](bundleConfig.jpg)

And add the following content:

```cs
using System.Web;
using System.Web.Optimization;

namespace MvcMusicStore
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/Site.css"));
        }
    }
}
```

Then register your bundles in Global.asax.cs by adding a call to `RegisterBundles()` inside the `Application_Start` method.

```cs
protected void Application_Start()
{
    AreaRegistration.RegisterAllAreas();

    WebApiConfig.Register(GlobalConfiguration.Configuration);
    FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
    RouteConfig.RegisterRoutes(RouteTable.Routes);
    BundleConfig.RegisterBundles(BundleTable.Bundles);
}
```

The Optimization package you installed above includes HTML helpers to let you render style and script bundles on the page. They are in the `System.Web.Optimization` namespace. To make the helpers available to you in Razor views, add `<add namespace="System.Web.Optimization"/>` to the list of namespaces inside the Web.config file in your Views folder.

```xml
<namespaces>
    <add namespace="System.Web.Mvc" />
    <add namespace="System.Web.Mvc.Ajax" />
    <add namespace="System.Web.Mvc.Html" />
    <add namespace="System.Web.Optimization"/>
    <add namespace="System.Web.Routing" />
</namespaces>
```

Finally, you're ready to include the style bundle in your page using the HTML helper `@Styles.Render()`

```html
<!DOCTYPE html>
<html>
<head>
    <title>@ViewBag.Title</title>
    @Styles.Render("~/Content/css")
</head>
<body>
    ...
</body>
</html>
```

Now let's run the application and see how our changes look on the Home page.

![](homePageWithStyle.jpg)

Let’s review what’s changed:
- The HomeController’s Index action method found and displayed the \Views\Home\Index.cshtmlView template, even though our code called “return View()”, because our View template followed the standard naming convention.
- The Home Page is displaying a simple welcome message that is defined within the \Views\Home\Index.cshtml view template.
- The Home Page is using our _Layout.cshtml template, and so the welcome message is contained within the standard site HTML layout.
- A stylesheet is included in the page using ASP.NET MVC bundling and minification support