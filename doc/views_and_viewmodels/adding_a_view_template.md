## Adding a View template

To use a view-template, we’ll change the HomeController Index method to return an ActionResult, and have it return View(), like below:

```cs
public class HomeController : Controller
{
    //
    // GET: /Home/
    public ActionResult Index()
    {
        return View();
    }
}
```
The above change indicates that instead of returned a string, we instead want to use a “View” to generate a result back.

We’ll now add an appropriate View template to our project. To do this we’ll position the text cursor within the Index action method, then right-click and select “Add View”. This will bring up the Add View dialog:

![](addView.jpg)
![](addViewDialog.jpg)

The “Add View” dialog allows us to quickly and easily generate View template files. By default the “Add View” dialog pre-populates the name of the View template to create so that it matches the action method that will use it. Because we used the “Add View” context menu within the Index() action method of our HomeController, the “Add View” dialog above has “Index” as the view name pre-populated by default. We don’t need to change any of the options on this dialog, so click the Add button.

When we click the Add button, Visual Web Developer will create a new Index.cshtml view template for us in the \Views\Home directory, creating the folder if doesn’t already exist.

![](solutionExplorerViewFolder.jpg)

The name and folder location of the “Index.cshtml” file is important, and follows the default ASP.NET MVC naming conventions. The directory name, \Views\Home, matches the controller - which is named HomeController. The view template name, Index, matches the controller action method which will be displaying the view.

ASP.NET MVC allows us to avoid having to explicitly specify the name or location of a view template when we use this naming convention to return a view. It will by default render the \Views\Home\Index.cshtml view template when we write code like below within our HomeController:

```cs
public class HomeController : Controller
{
    //
    // GET: /Home/
    public ActionResult Index()
    {
        return View();
    }
}
```

Visual Web Developer created and opened the “Index.cshtml” view template after we clicked the “Add” button within the “Add View” dialog. The contents of Index.cshtml are shown below.

```cs
@{
    ViewBag.Title = "Index";
}
<h2>Index</h2>
```

This view is using the Razor syntax, which is more concise than the Web Forms view engine used in ASP.NET Web Forms and previous versions of ASP.NET MVC.

The first three lines set the page title using ViewBag.Title. We’ll look at how this works in more detail soon, but first let’s update the text heading text and view the page. Update the `<h2>` tag to say “This is the Home Page” as shown below.

```cs
@{
    ViewBag.Title = "Index";
}
<h2>This is the Home Page</h2>
```
Running the application shows that our new text is visible on the home page.

![](homePage.jpg)