## Using a Layout for common site elements

Most websites have content which is shared between many pages: navigation, footers, logo images, stylesheet references, etc. The MVC convention is to put this shared content in a page called _Layout.cshtml inside a /Views/Shared folder. Create the folder by right clicking on the Views folder and selecting Add New -> Folder.

![](addSharedFolder.jpg)

Then add a new Razor view to that folder called _Layout.cshtml by right clicking the folder and selecting Add... -> View. Name the file _Layout and accept the other defaults.

![](_layoutDialog.jpg)

Now you can add the following Razor markup to this page.

```html
<!DOCTYPE html>
<html>
<head>
    <title>@ViewBag.Title</title>
</head>
<body>
    <div id="header">
        <h1>
            ASP.NET MVC MUSIC STORE</h1>
        <ul id="navlist">
            <li class="first"><a href="~/" id="current">Home</a></li>
            <li><a href="~/Store/">Store</a></li>
        </ul>
    </div>
    @RenderBody()
</body>
</html>
```

The content from our individual views will be displayed by the `@RenderBody()` command, and any common content that we want to appear outside of that can be added to the _Layout.cshtml markup. We’ll want our MVC Music Store to have a common header with links to our Home page and Store area on all pages in the site, so we’ll add that to the template directly above that `@RenderBody()` statement.

Notice that the href in the navlist begins with a `~`, ASP.Net MVC will automatically convert URLs beginning with ~ from a virtual path to an absolute path.

> If you've used previous versions of ASP.NET MVC you might be familiar with the `@Url.Content()'  or `@Href()` helpers, they're no longer necessary with Razor 2 in ASP.NET MVC 4.

To reference _Layout.cshtml from our individual views we can add a code snippet that refernces the layout. For example;

```cs
@{
    Layout = "~/Path/to/_Layout.cshtml";
}
``` 

However, because there's one layout that we want to use for *all* our views, we can put the same code in a special file named _ViewStart.cshtml inside the Views folder that configures all views in the application. Create a _ViewStart.cshtml file in the Views folder with the following content:

```cs
@{
    Layout = "~/Views/Shared/_Layout.cshtml";
}
```