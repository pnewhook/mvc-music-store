﻿using MvcMusicStore.Models;
using WebMatrix.WebData;

namespace MvcMusicStore
{
    public class MusicStoreDbContextInitializer
    {
        public static void InitializeDbContext()
        {
            var context = new MusicStoreContext();
            context.Database.Initialize(true);
            if (!WebSecurity.Initialized)
            {
                WebSecurity.InitializeDatabaseConnection("musicstoreconnection", "UserProfile", "UserId", "UserName", autoCreateTables: true);
            }
        }
    }
}