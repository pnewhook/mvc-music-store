﻿using System;
using System.Data.Entity.Infrastructure;
using System.Web.Security;
using MvcMusicStore.Models;
using WebMatrix.WebData;

namespace MvcMusicStore
{
    public class WebSecurityConfig
    {
        public static void InitializeWebSecurity()
        {
            try
            {
                using (var context = new MusicStoreContext())
                {
                    if (!context.Database.Exists())
                    {
                        // Create the SimpleMembership database without Entity Framework migration schema
                        ((IObjectContextAdapter)context).ObjectContext.CreateDatabase();
                    }
                }

                WebSecurity.InitializeDatabaseConnection("musicstoreconnection", "UserProfile", "UserId", "UserName", autoCreateTables: true);

                if (!Roles.RoleExists("admin"))
                {
                    WebSecurity.CreateUserAndAccount("administrator", "password1");
                    Roles.CreateRole("admin");
                    Roles.AddUserToRole("administrator", "admin");
                }
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("The ASP.NET Simple Membership database could not be initialized. For more information, please see http://go.microsoft.com/fwlink/?LinkId=256588", ex);
            }

        }
    }
}